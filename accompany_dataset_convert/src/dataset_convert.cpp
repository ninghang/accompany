
#include <ros/ros.h>

#include <tf/transform_listener.h>
#include <image_transport/image_transport.h>
#include <cv_bridge/cv_bridge.h>
#include <sensor_msgs/image_encodings.h>
using namespace cv_bridge;

#include <boost/program_options.hpp>
#include <boost/unordered_map.hpp>
#include <boost/foreach.hpp>
#include <boost/regex.hpp>
using namespace boost;

#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <opencv/cvwimage.h>
using namespace cv;

#include <limits>
#include <iostream>
#include <fstream>
#include <string>
#include <algorithm>
using namespace std;


typedef unordered_map<string, ofstream*> FilesMap;
FilesMap filesMap;
ofstream* getFile(string name)
{
  replace( name.begin(), name.end(), '/', '_'); // remove slash from file name
  FilesMap::const_iterator result=filesMap.find(name);
  if (result!=filesMap.end())
  {
    return result->second;
  }
  else
  {
    cout<<"creating file '"<<name<<"'"<<endl;
    ofstream* filep=new ofstream(name.c_str(),ofstream::out);
    filesMap[name]=filep;
    return filep;
  }
}

void closeFiles()
{
  cout<<"closing files"<<endl;
  for (FilesMap::iterator it=filesMap.begin();it!=filesMap.end();it++)
  {
    cout<<"closing '"<<it->first<<"'"<<endl;
    it->second->close();
  }
}

void tfCallback(const tf::tfMessage& tf)
{
  for (unsigned i=0;i<tf.transforms.size();i++)
  {
    regex exp("^/tracker/user_([0-9]*)/(.*)$");
    smatch matches;
    if (regex_match(tf.transforms[i].child_frame_id,matches,exp))
    {
      string id(matches[1].first, matches[1].second);
      string bodyPart(matches[2].first, matches[2].second);

      // save to file
      string filename=string("skeleton_")+id+string(".txt");
      ofstream* filep=getFile(filename);
      (*filep)<<tf.transforms[i].header.stamp<<" "
              <<bodyPart<<" "
              <<tf.transforms[i].transform.translation.x<<" "
              <<tf.transforms[i].transform.translation.y<<" "
              <<tf.transforms[i].transform.translation.z<<endl;

      // print
      cout<<filename<<": "<<tf.transforms[i].header.stamp<<" "
              <<bodyPart<<" "
              <<tf.transforms[i].transform.translation.x<<" "
              <<tf.transforms[i].transform.translation.y<<" "
              <<tf.transforms[i].transform.translation.z<<endl;
    }
  }
}

typedef unordered_map<string, int> FrameNrMap;
FrameNrMap frameNrMap;
int getFrameNr(String name)
{
  FrameNrMap::const_iterator result=frameNrMap.find(name);
  if (result!=frameNrMap.end())
  {
    int res=result->second+1;
    frameNrMap[name]=res;
    return res;
  }
  else
  {
    frameNrMap[name]=1;
    return 1;
  }
}

void imageCallback(const sensor_msgs::ImageConstPtr& image, const sensor_msgs::CameraInfoConstPtr& cameraInfo)
{
  // save image
  string name=image->header.frame_id;
  replace( name.begin(), name.end(), '/', '_'); // remove slash from file name

  int frameNr;
  stringstream ss;
  string encodingType = image->encoding;
  string filename;
  if (encodingType.compare("16UC1") == 0)
  {
    CvImageConstPtr cvImage=toCvShare(image,"16UC1");
    frameNr=getFrameNr(name+"depth");
    ss<<"depth_"<<std::setw(6)<<std::setfill('0')<<frameNr<<".png";
    imwrite(ss.str(),cvImage->image);
    filename="depth"+string("_time.txt");
  }
  else
  {
    CvImageConstPtr cvImage=toCvShare(image,"bgr8");
    frameNr=getFrameNr(name);
    ss<<name<<"_"<<std::setw(6)<<std::setfill('0')<<frameNr<<".png";
    imwrite(ss.str(),cvImage->image);
    filename=name+string("_time.txt");
  }

  // record association of frame number and timestamp

  ofstream* filep=getFile(filename);
  (*filep)<<std::setw(6)<<std::setfill('0')<<frameNr<<" "<<image->header.stamp<<endl;

  // print
  cout<<filename<<": "<<std::setw(6)<<std::setfill('0')<<frameNr<<" "<<image->header.stamp<<endl;
}


int main(int argc,char **argv)
{
  ros::init(argc, argv, "dataset_convert");
  vector<string> imageTopics;

  // handling command line arguments
  program_options::options_description optionsDescription
    ("Converts rosbag activity recognition dataset to Matlab format. "
     "It saves the body parts to a file for each skeleton and saves "
     "each images to file. Ros timestamps accompany all data elements.");
  optionsDescription.add_options()
    ("help,h","show help message")
    ("image-topics,i", program_options::value< vector<string> >(), "image topics")
    ;
  program_options::variables_map variablesMap;
  try
  {
    program_options::store(program_options::parse_command_line(argc, argv, optionsDescription),variablesMap);
    if (variablesMap.count("help")) {cout<<optionsDescription<<endl; return 0;}
    program_options::notify(variablesMap);
  }
  catch (const std::exception& e)
  {
    cerr<<""<<e.what()<<endl;
    return 1;
  }

  ros::NodeHandle n;
  image_transport::ImageTransport imageTransport(n);
  //ros::Subscriber humanDetectionsSub=n.subscribe<accompany_uva_msg::HumanDetections>("/humanDetections",10,&Tracker::processDetections,&tracker);
  ros::Subscriber tfSub= n.subscribe("tf", 0, tfCallback);

  if (variablesMap.count("image-topics"))
  {
    vector<string> imageTopics=variablesMap["image-topics"].as< vector<string> >();
    for (vector<string>::iterator it=imageTopics.begin();it!=imageTopics.end();it++)
    {
      cout<<"subscribe to topic:"<<*it<<endl;
      image_transport::CameraSubscriber imageSub1 = imageTransport.subscribeCamera(*it,1,imageCallback);
    }
  }

  ros::spin();

  closeFiles();
  return 0;
}
