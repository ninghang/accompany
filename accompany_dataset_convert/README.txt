

First see the topics in the ros bag files:

  rosbag list <ros-bag-files>

and identify which image topics should be saved.

Then start the data converter with your chosen image topics:

  rosrun accompany_dataset_convert dataset_convert -i <image-topic1> -i <image-topic2> 

Then play the ros bag files:

  rosbag play <ros-bag-files>

After which you find skeleton and image files in the directory where you started dataset_convert.


Example use:

# generate raw images
rosrun image_transport republish compressed in:=/camera_kitchen/image_raw raw out:=/camera_kitchen/image_raw
rosrun image_transport republish compressed in:=/camera_sofa/image_raw raw out:=/camera_sofa/image_raw

# set up converter
rosrun accompany_dataset_convert dataset_convert -i /cam3d/rgb/image_raw -i /camera_kitchen/image_raw -i /camera_sofa/image_raw

# play ros bag file
rosbag play tf_2014-04-25-14-30-02.bag object_detect_2014-04-25-14-30-04.bag kitchen_2014-04-25-14-30-01.bag sofa_2014-04-25-14-30-01.bag 



Addtionally, make a video from image using something like:

avconv -r 10  -i _robot_cam3d_link_%06d.png  test.mp4
